(ns starter.browser
  (:require [reagent.core :as r]
            [reagent.dom :as dom]
            [ol :as ol]
            ["ol/Map$default" :as Map]
            ["ol/View$default" :as View]
            ["ol/layer/Tile$default" :as TileLayer]
            ["ol/source/XYZ$default" :as XYZ]))

;; start is called by init and after code reloading finishes
(defn ^:dev/after-load start []
  (js/console.log "start"))

(defn init []
  ;; init is called ONCE when the page loads
  ;; this is called in the index.html and must be exported
  ;; so it is available even in :advanced release builds
  (js/console.log "init")
  (start))

;; this is called before any code is reloaded
(defn ^:dev/before-load stop []
  (js/console.log "stop"))

(defn ui-map []
  (let [map (r/atom nil)
        map-el (atom nil)
        rotation (r/atom 0.0)]

    (r/create-class
     {:display-name "open-layer-map"
      :component-did-mount
      (fn [this]
        (reset! map (new Map #js {:target (.-id @map-el) #_"ol-layer-map"
                                  :layers #js [(new TileLayer #js {:source
                                                                   (XYZ. #js {:url "https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png"})})]
                                  :view (new View #js {:center #js [0 0]
                                                       :rotation (/ 6 (.-PI js/Math))
                                                       :zoom 2})}))
        (prn "end"))

      :component-did-update (fn [this old-argv]
                              (-> (.setRotation (.getView @map) (* @rotation (/ (.-PI js/Math) 180)))))
      :reagent-render
      (fn []
        [:<>
         [:input {:on-change #(reset! rotation (-> % .-target .-value js/parseInt)) :default @rotation}]
         [:div (str (* @rotation (/ 180 (.-PI js/Math))))]
         [:div "test2"
          (str @map)
          [:div#open-layer-map.bg-green-500 {:ref #(reset! map-el %) :style {:width "600px" :height "600px"}}]]])})))

#_(-> (dom/createRoot (js/document.getElementById "app"))
      (.render [:div [ui-map]]))

(dom/render [:div [ui-map]] (js/document.getElementById "app"))
